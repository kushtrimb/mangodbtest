package repository; /**
 * Created by Kushtrim on 6/18/2017.
 */
import com.mangodb.entity.MangoDBApplication;
import com.mangodb.entity.User;
import com.mangodb.entity.UserRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MangoDBApplication.class)
public class TestApp {

    @Autowired
    private UserRepository userMongoRepository;

    @Before
    public void setUp() throws Exception {
        User user1 = new User("Kushtrim", 23);
        User user2 = new User("Fitim", 38);
        //save product, verify has ID value after save
        assertNull(user1.getId());
        assertNull(user2.getId());//null before save
        this.userMongoRepository.save(user1);
        this.userMongoRepository.save(user2);
        assertNotNull(user1.getId());
        assertNotNull(user2.getId());
    }

    @Test
    public void testFetchData() {
        /*Test data retrieval*/
        User userA = userMongoRepository.findByName("Fitim");
        assertNotNull(userA);
        assertEquals(38, userA.getAge());
        /*Get all products, list should only have two*/

        System.out.print("---> "+userA+" <---");

        Iterable<User> users = userMongoRepository.findAll();
        int count = 0;
        for (User p : users) {
            count++;
        }
        assertEquals(count, 2);
    }

    @Test
    public void testDataUpdate() {
        /*Test update*/
        User userB = userMongoRepository.findByName("Fitim");
        userB.setAge(40);
        userMongoRepository.save(userB);
        User userC = userMongoRepository.findByName("Fitim");
        assertNotNull(userC);
        assertEquals(40, userC.getAge());
    }

    @After
    public void tearDown() throws Exception {
        this.userMongoRepository.deleteAll();
    }

}