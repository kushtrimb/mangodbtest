package com.mangodb.entity;

/**
 * Created by Kushtrim on 6/18/2017.
 */
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, Integer> {
    User findByName(String name);
}