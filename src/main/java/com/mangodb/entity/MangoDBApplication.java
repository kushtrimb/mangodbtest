package com.mangodb.entity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Kushtrim on 6/18/2017.
 */
@SpringBootApplication
public class MangoDBApplication {

    public static void main(String[] args) {
        SpringApplication.run(MangoDBApplication.class, args);
    }
}